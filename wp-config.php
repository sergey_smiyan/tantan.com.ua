<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'tantan_db');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'root');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[N57arrA)G&-A/1I-yrf4+$| {Q~yNO#Zg&(H^xzs@yYR#l.4T5f6}XJ+gHQCtq?');
define('SECURE_AUTH_KEY',  '`cy|m/BL7ck@?$Qzt+-H5LT;#+HnmypwJaHHAuobR]U 4ylVzX]?-NXBtEHsY1rj');
define('LOGGED_IN_KEY',    'x|UKP*GJe<Eo&}SZFqsJJr$Q6-aa2 Q5 b`|=| AOQ*MvNl`9aXPk t0m`kX!Wyz');
define('NONCE_KEY',        'M)W4Sxr|*hl~&qMMt_~Cb.Fx#|&Oy3Sq4qn BS+G1fWVz]6 [$>MA_1r*2_/DfH#');
define('AUTH_SALT',        '?{A_]G>3w;;_g*[#d7y_JewI$5@Y)-X99E<}wg*~`)UBVOyxIlY&HN(2c;Ih#mw#');
define('SECURE_AUTH_SALT', '@[@rS~k,-iEOdh`/.&fqHm?&SGjrSY }#)d[ypW5>>jZ4VuH.wgKLxx=;c8.a/y5');
define('LOGGED_IN_SALT',   '.qQud(BnyY<T^Hh!-AZtvDVr-{rR)X|A:n:)|2YO)zIcFTNvSyni,x3_?^I|6Osz');
define('NONCE_SALT',       'Ce8&?hH3T0YS o#Ig9gi+~~J/=9M:lw|<F|N#Um?%bo,q$0}ufbk+|&`C5h1Ief/');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
