<footer class="site-footer">
    <div class="container">
        <div class="footer-top">
            <a href="<?= esc_url( home_url( '/' ) ) ?>" title="<?= esc_attr( get_bloginfo( 'name', 'display' ) ) ?>"
               class="footer-logo" rel="home">
                <img src="<?= get_template_directory_uri() ?>/images/logo.png" width="169" height="34"
                     alt="<?= esc_attr( get_bloginfo( 'name', 'display' ) ) ?>" class="footer-logo__img" />
            </a>
            <div class="footer-info">
                +38 (057) 728-27-89, +38 (099) 111-51-15<br>
                info@tantan.com.ua
            </div>
            <div class="copyright">
                Copyright &copy; <?= date('Y') ?> tantan.com.ua
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="design-bureau">Design by <a href="http://istardesign.com">iStar Design Bureau</a></div>
            <nav class="sub-navigation" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'footer-menu' ) ) ?>
            </nav>
        </div>
    </div>
</footer>

<?php wp_footer() ?>

</body>
</html>




