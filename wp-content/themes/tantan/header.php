<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ) ?>" />
    <meta name="viewport" content="width=device-width" />

    <title><?php wp_title( '::', true, 'right' ) ?></title>

    <?php wp_head() ?>

<!--    <link media="all" type="text/css" href="--><?//= get_template_directory_uri() ?><!--/css/common.less" rel="stylesheet/less">-->
    <link media="all" type="text/css" href="<?= get_template_directory_uri() ?>/css/common.css" rel="stylesheet">
</head>

<body <?php body_class() ?>>
	
<header class="site-header">
    <div class="topbar">
        <div class="container">
            <a href="mailto:info@tantan.com.ua" class="header-el header-mail">info@tantan.com.ua</a>
            <ul class="header-phone">
                <li class="header-el header-phone__1">+38 (057) 728-27-89</li>
                <li class="header-el header-phone__2">+38 (099) 111-51-15</li>
            </ul>
            <a href="#" class="header-el header-callback">Заказать обратный звонок</a>
        </div>
    </div>
    <div class="container">
        <a href="<?= esc_url( home_url( '/' ) ) ?>" title="<?= esc_attr( get_bloginfo( 'name', 'display' ) ) ?>"
           class="header-logo" rel="home">
            <img src="<?= get_template_directory_uri() ?>/images/logo.png" width="169" height="34"
                 alt="<?= esc_attr( get_bloginfo( 'name', 'display' ) ) ?>" class="header-logo__img" />
        </a>
        <nav class="main-navigation" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ) ?>
        </nav>
    </div>
</header>
