<?php
if ( post_password_required() )
	return;
?>

<?php
$comment_s = trim(strip_tags($_GET['cs']));
$comment_p = $_GET['cp'] ? (int)trim(strip_tags($_GET['cp'])) : 5;

$filterOptions = array(5, 10, 25);

?>

<div id="comments" class="comments-area">

    <?php if ( have_comments() ) : ?>

        <div class="comments-filter">
            <form method="get" action="">
                <label>
                    <input type="text" placeholder="Поиск по разделу" name="cs" class="form-control" value="<?= $comment_s ?>"/>
                </label>
                <label class="per-to-page">
                    <span>Записей на странице:</span>
                    <select id="page-numbers" name="cp" class="form-control">
                    <?php foreach ($filterOptions as $option): ?>
                        <option value="<?= $option ?>"<?= (($option === $comment_p) ? ' selected="selected"' : '') ?>>
                            <?= $option ?>
                        </option>
                    <?php endforeach  ?>
                    </select>
                </label>
            </form>
        </div>

        <?php $comments = get_comments( array( 'search' => $comment_s, 'order' => 'ASC', 'status' => 'approve',  'post_id' => get_the_ID() ) ); ?>

		<ol class="comment-lst">
			<?php wp_list_comments( array( 'callback' => 'dz_comment', 'max_depth' => 2, 'per_page'  => $comment_p ) ) ?>
		</ol>

        <?php if(get_comment_pages_count() > 1): ?>
        <div class="pagination-list">
            <div class="pagination-list__1">
                <a href="<?= get_comment_link('', 1) ?>" class="first page-numbers"></a>
                <?php paginate_comments_links(
                    array(
                        'prev_text'    => '',
                        'next_text'    => '',
                        'add_fragment' => ''
                    )
                ) ?>
            </div>
            <div class="pagination-list__2">
                <a href="<?= get_comment_link('', get_comment_pages_count()) ?>" class="last page-numbers"></a>
            </div>
        </div>
        <?php endif; ?>


	<?php endif ?>

	<?php comment_form( array(
        'comment_field'        => '<label><textarea name="comment" class="form-control" placeholder="Текст сообщения"></textarea></label>',
        'comment_notes_before' => '',
        'comment_notes_after'  => '',
        'id_form'              => 'comment-form',
        'label_submit'         => 'Отправить',
        'class_submit'         => 'btn btn-custom',
        'title_reply'          => 'Оставьте отзыв',
    )) ?>

</div>